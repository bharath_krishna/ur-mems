package main

import "time"

// AlbumMedias holds individual media data
type AlbumMediaList struct {
	MediaItems *[]*MediaItem `json:"mediaItems"`
}
type PhotoVideo struct {
	Status          string  `json:"status"`
	CameraMake      string  `json:"cameraMake"`
	CameraModel     string  `json:"cameraModel"`
	FocalLength     float64 `json:"focalLength"`
	ApertureFNumber float64 `json:"apertureFNumber"`
	IsoEquivalent   int     `json:"isoEquivalent"`
}
type MediaMetadata struct {
	CreationTime time.Time  `json:"creationTime"`
	Width        string     `json:"width"`
	Height       string     `json:"height"`
	Photo        PhotoVideo `json:"photo"`
}
type MediaItem struct {
	ID            string        `json:"id"`
	ProductURL    string        `json:"productUrl"`
	BaseURL       string        `json:"baseUrl"`
	MimeType      string        `json:"mimeType"`
	Filename      string        `json:"filename"`
	MediaMetadata MediaMetadata `json:"mediaMetadata,omitempty"`
}
