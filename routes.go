package main

import "github.com/gin-gonic/gin"

// Routes creates endpoints of server
func (s *Server) Routes(r *gin.Engine) {
	profileRoutes := r.Group("/profile")
	profileRoutes.Use(s.FetchProfile())
	{
		profileRoutes.GET("/", s.Profile)
		profileRoutes.GET("/basic", s.Basic)
		profileRoutes.GET("/experience", s.Experience)
		profileRoutes.GET("/experience/:id", s.Experience)
		profileRoutes.GET("/education", s.Education)
		profileRoutes.GET("/education/:id", s.Education)
		profileRoutes.GET("/personal", s.Personal)
		profileRoutes.GET("/skills", s.Skills)
		profileRoutes.GET("/languages", s.Languages)
		profileRoutes.GET("/contacts", s.Contacts)
	}
	r.GET("/testgoogle", s.TestGoogle)
	r.GET("/google/callback", s.GoogleCallback)
	authURLs := r.Group("/api")
	authURLs.Use(s.AuthenticateUser())
	{
		authURLs.POST("/authn/token", s.TokenHandler)
		authURLs.GET("/", s.Welcome)
		authURLs.GET("/albums", s.ListAlbums)
		authURLs.GET("/album/:id/medias", s.ListAlbumMedia)

		authProfileRoutes := authURLs.Group("/profile")
		authProfileRoutes.Use(s.FetchProfile())

		authProfileRoutes.PATCH("/basic", s.UpadteBasic)
		authProfileRoutes.PATCH("/experience", s.Experience)
		authProfileRoutes.PATCH("/education/:id", s.UpdateEducation)
		authProfileRoutes.PATCH("/personal", s.UpdatePersonal)
		authProfileRoutes.PATCH("/skills", s.UpdateSkills)
		authProfileRoutes.PATCH("/languages", s.UpdateLanguages)
		authProfileRoutes.PATCH("/contacts", s.UpdateContacts)
	}
}
