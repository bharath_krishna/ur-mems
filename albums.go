package main

// Album holds
type Album struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Path string `json:"path,omitempty"`
}

// GoogleAlbums holds google photo album details
type GoogleAlbums struct {
	Albums        []*GoogleAlbum `json:"albums"`
	NextPageToken string         `json:"nextPageToken"`
}

// GoogleAlbum holds single album details
type GoogleAlbum struct {
	ID                    string `json:"id"`
	Title                 string `json:"title"`
	ProductURL            string `json:"productUrl"`
	IsWriteable           bool   `json:"isWriteable"`
	MediaItemsCount       string `json:"mediaItemsCount"`
	CoverPhotoBaseURL     string `json:"coverPhotoBaseUrl"`
	CoverPhotoMediaItemID string `json:"coverPhotoMediaItemId"`
}
