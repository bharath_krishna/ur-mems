ARG GO_VERSION=1.12.6
FROM golang:${GO_VERSION} as builder
LABEL maintainer="Bharath Krishna <bharath.chakravarthi@gmail.com>"
WORKDIR /go/src/app
COPY . .
RUN go get -d -v ./...
RUN CGO_ENABLED=0 go build -installsuffix 'static' -o /app .

FROM scratch AS final
COPY --from=builder /app /app

EXPOSE 8088
ENTRYPOINT ["/app"]