package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/oauth2"
)

// Server holds client config, logger and server routing details
type Server struct {
	config   *Config
	app      *gin.Engine
	O2Config oauth2.Config
	provider *oidc.Provider
	mongo    *mongo.Client
}

func setupServer() *Server {
	config := NewDefaultConfig()
	server := createNewServer(config)
	return server
}

var (
	dbName         = "kbharathk"
	collectionName = "profile"
)

func isExists(arr []string, name string) bool {
	for _, el := range arr {
		if el == name {
			return true
		}
	}
	return false
}

// SetupDatabase create database and collection in the given mongodb
func SetupDatabase(client *mongo.Client) bool {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	dbs, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		panic(err)
	}
	if isExists(dbs, dbName) {
		collections, err := client.Database(dbName).ListCollectionNames(ctx, bson.M{})
		if err != nil {
			return false
		}
		if isExists(collections, collectionName) {
			return true
		}
	}
	resumeFile, _ := ioutil.ReadFile("profile.json")
	err = json.Unmarshal([]byte(resumeFile), &profile)
	if err != nil {
		return false
	}

	collection := client.Database(dbName).Collection(collectionName)
	_, err = collection.InsertOne(ctx, profile)
	if err != nil {
		return false
	}
	return true
}

func createNewServer(config *Config) *Server {
	provider, err := oidc.NewProvider(oauth2.NoContext, config.AuthHost+"/auth/realms/demo")
	if err != nil {
		panic(err)
	}
	config.provider = provider

	// For three legged authentication flow
	o2Config := oauth2.Config{
		ClientID:     config.ClientID,
		ClientSecret: config.ClientSecret,
		RedirectURL:  os.Getenv("CALLBACK_URL"),
		Endpoint:     provider.Endpoint(),
		Scopes:       []string{"profile", "email"},
	}

	mongoURL := fmt.Sprintf("mongodb://%s:%s@%s:%s",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"))
	clientOptions := options.Client().ApplyURI(mongoURL)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		panic(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	if !SetupDatabase(client) {
		panic(fmt.Errorf("Database is not setup"))
	}

	s := &Server{
		O2Config: o2Config,
		config:   config,
		provider: config.provider,
		mongo:    client,
	}

	s.app = s.setupRoutes()
	return s
}

func (s *Server) setupRoutes() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(s.AllowOriginRequests())
	s.Routes(r)
	return r
}

// Run runs the server
func (s *Server) Run() {
	s.app.Run(s.config.ServerAddr)
}
