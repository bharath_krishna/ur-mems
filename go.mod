module ur-mems

go 1.12

require (
	github.com/astaxie/beego v1.12.0
	github.com/coreos/go-oidc v2.0.0+incompatible
	github.com/gemcook/pagination-go v0.2.0
	github.com/gin-gonic/contrib v0.0.0-20190526021735-7fb7810ed2a0
	github.com/gin-gonic/gin v1.4.0
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/square/go-jose.v2 v2.3.1 // indirect
)
