package main

import (
	"os"

	"github.com/coreos/go-oidc"
)

// Config holds server configs
type Config struct {
	ServerAddr    string         `name:"addr" usage:"IP address and port to listen on" env:"ADDRESS"`
	AuthHost      string         `name:"auth-host" usage:"AuthN/AuthZ host" env:"AUTH_HOST"`
	KeycloakRealm string         `name:"keycloak-realm" usage:"Keycloak realm used to authenticate to the oauth service" env:"KEYCLOAK_REALM"`
	ClientID      string         `name:"client-id" usage:"client id used to authenticate to the oauth service" env:"CLIENT_ID"`
	ClientSecret  string         `name:"client-secret" usage:"client secret used to authenticate to the oauth service" env:"CLIENT_SECRET"`
	provider      *oidc.Provider `name:"oidc provider"`
}

// NewDefaultConfig create default configs
func NewDefaultConfig() *Config {
	return &Config{
		ServerAddr:    ":8088",
		KeycloakRealm: os.Getenv("KEYCLOAK_REALM"),
		ClientID:      os.Getenv("CLIENT_ID"),
		ClientSecret:  os.Getenv("CLIENT_SECRET"),
		AuthHost:      os.Getenv("AUTH_HOST"),
	}
}
