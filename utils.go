package main

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
)

func getBasicCreds(c *gin.Context) (string, string, error) {
	token, err := getAuthParts(c)
	if err != nil {
		return "", "", err
	}
	decoded, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		return "", "", fmt.Errorf("Invali basic auth")
	}
	parts := strings.Split(string(decoded), ":")
	return parts[0], parts[1], nil
}

func getAuthParts(c *gin.Context) (string, error) {
	authH := c.GetHeader("Authorization")
	if authH == "" {
		return "", fmt.Errorf("Please provide user credentials")
	}
	parts := strings.Split(authH, " ")
	if len(parts) != 2 {
		return "", fmt.Errorf("Invali basic auth")
	}
	return parts[1], nil
}

// verifyToken verifies the user provided request token
func verifyToken(token string, c *gin.Context, s *Server) (*oidc.IDToken, error) {
	verifier := s.config.provider.Verifier(&oidc.Config{
		ClientID: s.config.ClientID,
	})
	verifiedToken, err := verifier.Verify(c, token)
	if err != nil {
		return verifiedToken, fmt.Errorf("failed to verify access token: " + err.Error())
	}
	// add more verification methods
	return verifiedToken, nil
}
