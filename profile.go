package main

type AutoGeneratedProfile struct {
	Basic      map[string]string   `json:"basic"`
	Experience []*ExpItems         `json:"experience"`
	Education  []map[string]string `json:"education"`
	Personal   Personal            `json:"personal"`
	Skills     map[string]string   `json:"skills"`
	Contatcs   map[string]string   `json:"contatcs"`
	Languages  []string            `json:"languages"`
}
type ExpItems struct {
	ID          string   `json:"id"`
	Designation string   `json:"designation"`
	From        string   `json:"from"`
	To          string   `json:"to"`
	Company     string   `json:"company"`
	RNR         []string `json:"rnr"`
}
type EduItems struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	From      string `json:"from"`
	To        string `json:"to"`
	Institute string `json:"institute"`
}
type Basic struct {
	Name        string `json:"name"`
	Designation string `json:"designation"`
	Description string `json:"description"`
}
type Personal struct {
	Phone   string `json:"phone" bson:"phone"`
	Address string `json:"address" bson:"address"`
	Mail    string `json:"mail" bson:"mail"`
}
type Skills struct {
	Python     int `json:"python"`
	Aiohttp    int `json:"aiohttp"`
	Golang     int `json:"golang"`
	GoGin      int `json:"Go Gin"`
	Doccker    int `json:"Doccker"`
	Kubernetes int `json:"Kubernetes"`
}
type Contatcs struct {
	Linkedin  string `json:"linkedin"`
	Bitbucket string `json:"bitbucket"`
	Mail      string `json:"mail"`
}
